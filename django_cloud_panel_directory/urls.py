from django.urls import path

from . import views

urlpatterns = [
    path("user_account/settings", views.user_account_settings),
    path(
        "user_account/settings/change-profile-picture",
        views.modal_change_profile_picture,
    ),
    path("realms", views.list_realms),
    path("users", views.list_users),
    path("users/create", views.modal_users_create, name="modal_users_create"),
    path("user/<str:guid>", views.show_user),
    path("user/<str:guid>/reset-pw", views.modal_user_reset_pw),
    path("user/<str:guid>/delete", views.modal_user_delete, name="modal_user_delete"),
    path(
        "user/<str:guid>/groups",
        views.modal_user_manage_groups,
        name="modal_user_manage_groups",
    ),
    path("groups", views.list_groups),
    path("groups/create", views.modal_groups_create, name="modal_groups_create"),
    path("group/<str:group_guid>", views.show_group),
    path("group/<str:group_guid>/delete", views.modal_group_delete),
    path(
        "group/<str:group_guid>/members",
        views.modal_group_manage_members,
        name="modal_group_manage_members",
    ),
    path("computers", views.list_computers),
    path(
        "computers/create", views.modal_computers_create, name="modal_computers_create"
    ),
    path("computer/<str:guid>", views.show_computer),
    path("computer/<str:guid>/join-script", views.modal_computer_join_script),
    path("computer/<str:guid>/delete", views.modal_computer_delete),
    path(
        "computer/<str:guid>/groups",
        views.modal_computer_manage_groups,
        name="modal_computer_manage_groups",
    ),
    path("contacts", views.list_contacts),
    path("contacts/<int:contact_id>", views.show_contact),
]
