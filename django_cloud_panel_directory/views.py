from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_directory.django_cloud_panel_directory.forms import *
from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_directory_backend_client.django_directory_backend_client.func import *
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)
from django_session_ldap_attributes.django_session_ldap_attributes.snippets import (
    get_mdat_customer_for_user,
)


@login_required
def user_account_settings(request):
    template = loader.get_template(
        "django_cloud_panel_directory/user_account_settings.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("User Settings")
    template_opts["content_title_sub"] = request.user.get_full_name()

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        directory_user_password_own_change_form = DirectoryUserPasswordOwnChangeForm(
            request.POST
        )

        # check whether it's valid:
        if directory_user_password_own_change_form.is_valid():
            # check if new passwords match
            if (
                not directory_user_password_own_change_form.cleaned_data["new_password"]
                == directory_user_password_own_change_form.cleaned_data["new_password2"]
            ):
                messages.error(request, _("Passwords did not match."))
                return HttpResponseRedirect("/directory/user_account/settings")

            upn = request.user.userldapattributes.json_data["userPrincipalName"][0]

            api_response = directory_backend_change_user_password(
                mdat.id,
                upn,
                directory_user_password_own_change_form.cleaned_data["new_password"],
                directory_user_password_own_change_form.cleaned_data["old_password"],
            )

            if api_response is False:
                messages.error(
                    request,
                    _(
                        "Password change failed. Please check UPN as well as password complexity and try again."
                    ),
                )
            else:
                messages.success(request, _("Your password has been updated."))
                return HttpResponseRedirect("/dashboard")

    # if a GET (or any other method) we'll create a blank form
    directory_user_password_own_change_form = DirectoryUserPasswordOwnChangeForm()

    template_opts["directory_user_password_own_change_form"] = (
        directory_user_password_own_change_form
    )

    directory_user_profile_information_form = DirectoryUserProfileInformation()
    for data_element in request.user.userldapattributes.json_data:
        if data_element.lower() in directory_user_profile_information_form.fields:
            directory_user_profile_information_form.fields[
                data_element.lower()
            ].initial = request.user.userldapattributes.json_data[data_element][0]

    template_opts["directory_user_profile_information_form"] = (
        directory_user_profile_information_form
    )

    return HttpResponse(template.render(template_opts, request))


@login_required
def modal_change_profile_picture(request):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_change_profile_picture.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("User Settings")
    template_opts["content_title_sub"] = _("Change profile picture")
    if request.method == "POST":
        # TODO: send picture to API
        return HttpResponseRedirect("/directory/user_account/settings")

    # TODO: change URL to user profile picture
    template_opts["image_path"] = "/static/global_assets/images/placeholders/user.jpg"

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def list_tenants(request):
    template = loader.get_template("django_cloud_panel_directory/list_tenants.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Tenants")
    template_opts["content_title_sub"] = _("Overview")

    template_opts["menu_groups"] = get_nav(request)

    # tenants the user has access to:
    all_tenants = list()
    template_opts["all_tenants"] = all_tenants

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def list_users(request):
    template = loader.get_template("django_cloud_panel_directory/list_users.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Users")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["all_users"] = directory_backend_get_users(mdat.id)
    for index, user in enumerate(template_opts["all_users"]):
        template_opts["all_users"][index]["X_SID"] = user["X-SID"]
        template_opts["all_users"][index]["X_GUID"] = user["X-GUID"]

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def show_user(request, guid):
    template = loader.get_template("django_cloud_panel_directory/show_user.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("User")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["X_GUID"] = guid

    user_attributes = directory_backend_get_user(mdat.id, guid)

    template_opts["tenant_user_attributes"] = user_attributes

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def list_groups(request):
    template = loader.get_template("django_cloud_panel_directory/list_groups.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Groups")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["all_groups"] = directory_backend_get_groups(mdat.id)
    static_values = directory_backend_get_static_values(mdat.id)

    for index, group in enumerate(template_opts["all_groups"]):
        template_opts["all_groups"][index]["X_SID"] = group["X-SID"]
        template_opts["all_groups"][index]["X_GUID"] = group["X-GUID"]
        template_opts["all_groups"][index]["group_type"] = static_values["group_types"][
            group["groupType"][0]
        ]

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def show_group(request, group_guid):
    template = loader.get_template("django_cloud_panel_directory/show_group.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Group")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["group_guid"] = group_guid

    group_attributes = directory_backend_get_group(
        mdat.id,
        group_guid,
    )

    template_opts["tenant_group_attributes"] = group_attributes

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def list_computers(request):
    template = loader.get_template("django_cloud_panel_directory/list_computers.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Computers")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["all_computers"] = directory_backend_get_computers(mdat.id)
    for index, computer in enumerate(template_opts["all_computers"]):
        template_opts["all_computers"][index]["X_SID"] = computer["X-SID"]
        template_opts["all_computers"][index]["X_GUID"] = computer["X-GUID"]

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def show_computer(request, guid):
    template = loader.get_template("django_cloud_panel_directory/show_computer.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Computer")

    template_opts["menu_groups"] = get_nav(request)

    template_opts["guid"] = guid

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["tenant_computer_attributes"] = directory_backend_get_computer(
        mdat.id,
        guid,
    )

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_computer_join_script(request, guid):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_computer_join_script.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Computer Join Script") + " - " + guid

    template_opts["guid"] = guid

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_user_reset_pw(request, guid):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_user_reset_pw.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Reset User-Password") + " - " + guid

    template_opts["X-GUID"] = guid

    mdat = get_mdat_customer_for_user(request.user)

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        directory_user_password_change_form = DirectoryUserPasswordChangeForm(
            request.POST
        )

        # check whether it's valid:
        if directory_user_password_change_form.is_valid():
            # check if new passwords match
            if (
                not directory_user_password_change_form.cleaned_data["new_password"]
                == directory_user_password_change_form.cleaned_data["new_password2"]
            ):
                messages.error(request, _("Passwords did not match."))
                return HttpResponseRedirect("/directory/users")

            api_response = directory_backend_change_user_password(
                mdat.id,
                guid,
                directory_user_password_change_form.cleaned_data["new_password"],
            )

            if api_response is False:
                messages.error(
                    request,
                    _(
                        "Password change failed. Please check UPN as well as password complexity and try again."
                    ),
                )
            else:
                messages.success(request, _("The password has been updated."))
                return HttpResponseRedirect("/directory/users")

    # if a GET (or any other method) we'll create a blank form
    directory_user_password_change_form = DirectoryUserPasswordChangeForm()

    template_opts["directory_user_password_change_form"] = (
        directory_user_password_change_form
    )

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_user_delete(request, guid):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_user_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("User Delete") + " - " + guid

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["X_GUID"] = guid

    if request.method == "POST":
        directory_user_delete_form = DirectoryUserDeleteForm(request.POST)

        if directory_user_delete_form.is_valid():
            api_response = directory_backend_delete_user(mdat.id, guid)

            if api_response is False:
                messages.error(request, _("User deletion failed. Please try again."))
                return HttpResponseRedirect("/directory/users")

            messages.success(request, _("User deleted successfully: ") + guid)

            return HttpResponseRedirect("/directory/users")

    directory_user_delete_form = DirectoryUserDeleteForm(initial={"guid": guid})

    template_opts["directory_user_delete_form"] = directory_user_delete_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_users_create(request):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_users_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("User Create")

    mdat = get_mdat_customer_for_user(request.user)

    if request.method == "POST":
        directory_create_user_form = DirectoryUserCreateForm(
            request.POST, user=request.user
        )

        if directory_create_user_form.is_valid():
            user_data = dict()

            # Pull list of domains
            available_domains = directory_backend_get_authdomain(mdat.id, mdat.org_tag)
            domain_allowed = False

            for domain in available_domains:
                if int(domain["domainID"]) == int(
                    directory_create_user_form.cleaned_data["domain"]
                ):
                    user_data["domain"] = domain["domain"]
                    domain_allowed = True

            if not domain_allowed:
                messages.error(
                    request, _("You are not allowed to use this realm / domain.")
                )
                return HttpResponseRedirect("/directory/users")

            user_data["first_name"] = directory_create_user_form.cleaned_data[
                "first_name"
            ]
            user_data["last_name"] = directory_create_user_form.cleaned_data[
                "last_name"
            ]
            user_data["display_name"] = directory_create_user_form.cleaned_data[
                "display_name"
            ]
            user_data["username"] = directory_create_user_form.cleaned_data["username"]

            new_user = directory_backend_create_user(mdat.id, mdat.org_tag, user_data)

            if new_user is False:
                messages.error(
                    request,
                    _("User creation failed. Please check syntax and try again."),
                )
                return HttpResponseRedirect("/directory/users")

            upn = user_data["username"] + "@" + user_data["domain"]

            messages.success(request, _("User created successfully: ") + upn)

            return HttpResponseRedirect("/directory/user/" + upn)

    directory_create_user_form = DirectoryUserCreateForm(user=request.user)

    template_opts["directory_create_user_form"] = directory_create_user_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_group_delete(request, group_guid):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_group_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Group Delete") + " - " + group_guid

    template_opts["group_id"] = group_guid

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_groups_create(request):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_groups_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Group Create")

    if request.method == "POST":
        directory_create_group_form = DirectoryCreateGroupForm(request.POST)

        if directory_create_group_form.is_valid():
            # TODO: implement create groups api
            messages.success(request, _("Group created successfully."))
            return HttpResponseRedirect("/directory/groups")
        else:
            messages.error(request, _("Group creation failed."))

    directory_create_group_form = DirectoryCreateGroupForm()

    template_opts["directory_create_group_form"] = directory_create_group_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_group_manage_members(request, group_guid):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_group_manage_members.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Group Members")

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["group_guid"] = group_guid

    group_attributes = directory_backend_get_group(
        mdat.id,
        group_guid,
    )

    template_opts["tenant_group_attributes"] = group_attributes

    if request.method == "POST":
        group_members_form = DirectoryGroupMembersForm(
            request.POST, user=request.user, group_attributes=group_attributes
        )

        if group_members_form.is_valid():
            if directory_backend_assign_groups_to_tenant_group(
                mdat.id,
                mdat.org_tag,
                group_guid,
                group_members_form.cleaned_data.get("members"),
            ):
                messages.success(request, _("Group members saved successfully."))
                return HttpResponseRedirect("/directory/groups")
            else:
                messages.error(
                    request, _("Group member operation failed. Please try again.")
                )
        else:
            messages.error(
                request, _("Group member operation failed. Please try again.")
            )

    # if a GET (or any other method) we'll create a blank form
    group_members_form = DirectoryGroupMembersForm(
        user=request.user, group_attributes=group_attributes
    )

    template_opts["group_members_form"] = group_members_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_user_manage_groups(request, guid):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_user_manage_groups.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Group Memberships")

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["X-GUID"] = guid

    user_attributes = directory_backend_get_user(mdat.id, guid)

    template_opts["tenant_user_attributes"] = user_attributes

    if request.method == "POST":
        user_groups_form = DirectoryUserGroupMembershipForm(
            request.POST, user=request.user, user_attributes=user_attributes
        )

        if user_groups_form.is_valid():
            # TODO: add new requested form function
            if directory_backend_assign_groups_to_tenant_user(
                mdat.id, mdat.org_tag, guid, user_groups_form.cleaned_data.get("groups")
            ):
                messages.success(request, _("Group membership saved successfully."))
                return HttpResponseRedirect("/directory/users")
            else:
                messages.error(
                    request, _("Group membership operation failed. Please try again.")
                )
        else:
            messages.error(
                request, _("Group membership operation failed. Please try again.")
            )

    # if a GET (or any other method) we'll create a blank form
    user_groups_form = DirectoryUserGroupMembershipForm(
        user=request.user, user_attributes=user_attributes
    )

    template_opts["user_groups_form"] = user_groups_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_computer_delete(request, guid):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_computer_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Computer Delete") + " - " + guid

    template_opts["guid"] = guid

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_computers_create(request):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_computers_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Computer Create")

    if request.method == "POST":
        directory_create_computer_form = DirectoryCreateComputerForm(request.POST)

        if directory_create_computer_form.is_valid():
            # TODO: implement create computer api
            messages.success(request, _("Computer created successfully."))
            return HttpResponseRedirect("/directory/computers")
        else:
            messages.error(request, _("Computer creation failed."))

    directory_create_computer_form = DirectoryCreateComputerForm

    template_opts["directory_create_computer_form"] = directory_create_computer_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def modal_computer_manage_groups(request, computer_sid):
    template = loader.get_template(
        "django_cloud_panel_directory/modal_computer_manage_groups.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Group Memberships")

    mdat = get_mdat_customer_for_user(request.user)

    computer_attributes = directory_backend_get_computer(
        mdat.id,
        computer_sid,
    )

    template_opts["tenant_computer_attributes"] = computer_attributes

    if request.method == "POST":
        computer_groups_form = DirectoryComputerGroupMembershipForm(
            request.POST, user=request.user, computer_attributes=computer_attributes
        )

        if computer_groups_form.is_valid():
            if directory_backend_assign_groups_to_tenant_computer(
                mdat.id,
                mdat.org_tag,
                computer_name,
                computer_groups_form.cleaned_data.get("groups"),
            ):
                messages.success(request, _("Group membership saved successfully."))
                return HttpResponseRedirect("/directory/computers")
            else:
                messages.error(
                    request, _("Group membership operation failed. Please try again.")
                )
        else:
            messages.error(
                request, _("Group membership operation failed. Please try again.")
            )

    # if a GET (or any other method) we'll create a blank form
    computer_groups_form = DirectoryComputerGroupMembershipForm(
        user=request.user, computer_attributes=computer_attributes
    )

    template_opts["computer_groups_form"] = computer_groups_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def list_realms(request):
    template = loader.get_template("django_cloud_panel_directory/list_realms.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Realms")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    template_opts["all_realms"] = directory_backend_get_authdomain(mdat.id)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def list_contacts(request):
    template = loader.get_template("django_cloud_panel_directory/list_contacts.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Contacts")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DIRECTORY")
def show_contact(request, contact_id):
    template = loader.get_template("django_cloud_panel_directory/show_contact.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Directory")
    template_opts["content_title_sub"] = _("Contact")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))
