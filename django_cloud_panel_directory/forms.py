from braces.forms import UserKwargModelFormMixin
from django import forms
from django.utils.translation import gettext_lazy as _

from django_session_ldap_attributes.django_session_ldap_attributes.snippets import (
    get_mdat_customer_for_user,
)
from django_directory_backend_client.django_directory_backend_client.func import *


def get_computer_types():
    choices = list()

    for key, value in directory_backend_get_static_values()["computer_types"].items():
        choices.append((key, _(value)))

    return choices


def get_computer_countries():
    choices = list()

    for key, value in directory_backend_get_static_values()[
        "computer_countries"
    ].items():
        choices.append((key, _(value)))

    return choices


class DirectoryUserPasswordChangeForm(forms.Form):
    new_password = forms.CharField(
        label=_(""),
        widget=forms.PasswordInput(
            attrs={
                "autocomplete": "new-password",
                "placeholder": "New Password (again)",
                "class": "form-control",
            }
        ),
        max_length=100,
    )
    new_password2 = forms.CharField(
        label=_(""),
        widget=forms.PasswordInput(
            attrs={
                "autocomplete": "new-password",
                "placeholder": "Retype Password",
                "class": "form-control",
            }
        ),
        max_length=100,
    )


class DirectoryUserPasswordOwnChangeForm(forms.Form):
    old_password = forms.CharField(
        label=_(""),
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control",
                "placeholder": "Old Password",
            }
        ),
        max_length=100,
    )
    new_password = forms.CharField(
        label=_(""),
        widget=forms.PasswordInput(
            attrs={
                "autocomplete": "new-password",
                "placeholder": "New Password",
                "class": "form-control",
            }
        ),
        max_length=100,
    )
    new_password2 = forms.CharField(
        label=_(""),
        widget=forms.PasswordInput(
            attrs={
                "autocomplete": "new-password",
                "placeholder": "Retype Password",
                "class": "form-control",
            }
        ),
        max_length=100,
    )


class DirectoryUserDeleteForm(forms.Form):
    guid = forms.CharField(label=_("X-GUID"), widget=forms.HiddenInput, max_length=250)


class DirectoryUserCreateForm(UserKwargModelFormMixin, forms.Form):
    first_name = forms.CharField(
        label=_(""),
        widget=forms.TextInput(
            attrs={
                "class": "form-control col-lg-6",
                "placeholder": _("Firstname"),
                "onchange": "concat_names()",
            }
        ),
        required=False,
    )
    last_name = forms.CharField(
        label=_(""),
        widget=forms.TextInput(
            attrs={
                "class": "form-control col-lg-6",
                "placeholder": _("Lastname"),
                "onchange": "concat_names()",
            }
        ),
        required=False,
    )
    display_name = forms.CharField(
        label=_(""),
        widget=forms.TextInput(
            attrs={"class": "form-control col-lg-12", "placeholder": _("Display Name")}
        ),
        required=True,
    )
    username = forms.CharField(
        label=_(""),
        widget=forms.TextInput(
            attrs={"class": "form-control col-lg-6", "placeholder": _("Username")}
        ),
        required=True,
    )
    domain = forms.ChoiceField(
        label="", widget=forms.Select(attrs={"class": "form-control col-lg-5"})
    )

    def __init__(self, *args, **kwargs):
        super(DirectoryUserCreateForm, self).__init__(*args, **kwargs)

        # make sure, under no circumstances, user is None
        assert self.user is not None

        mdat = get_mdat_customer_for_user(self.user)

        domain_choices = list()

        domain_data = directory_backend_get_authdomain(mdat.id)

        for domain in domain_data:
            if domain["isDefault"]:
                self.initial["domain"] = domain["domain"]

            domain_choices.append((domain["domainID"], domain["domain"]))

        self.fields["domain"].choices = domain_choices


class DirectoryCreateGroupForm(forms.Form):
    group_name = forms.CharField(
        label=_(""),
        widget=forms.TextInput(
            attrs={"class": "form-control", "placeholder": _("New Group-name")}
        ),
        required=True,
    )


class DirectoryCreateComputerForm(forms.Form):
    type = forms.ChoiceField(
        choices=get_computer_types, widget=forms.Select(attrs={"class": "form-control"})
    )
    country = forms.ChoiceField(
        choices=get_computer_countries,
        widget=forms.Select(attrs={"class": "form-control"}),
    )


class DirectoryGroupMembersForm(UserKwargModelFormMixin, forms.Form):
    members = forms.MultipleChoiceField(
        required=False,
        widget=forms.SelectMultiple,
        label="",
    )

    def __init__(self, *args, **kwargs):
        self.group_attributes = kwargs.pop("group_attributes", None)

        super(DirectoryGroupMembersForm, self).__init__(*args, **kwargs)

        # make sure, under no circumstances, user is None
        assert self.user is not None

        mdat = get_mdat_customer_for_user(self.user)

        members_choices = list()
        members_selected = list()

        org_users = directory_backend_get_users(mdat.id)
        org_groups = directory_backend_get_groups(mdat.id)
        org_computers = directory_backend_get_computers(mdat.id)

        for org_user in org_users:
            members_choices.append(
                (
                    "user_" + org_user["objectGUID"][0],
                    _("User") + ": " + org_user["displayName"][0],
                )
            )
            if org_user["distinguishedName"][0].upper() in (
                dn.upper() for dn in self.group_attributes["member"]
            ):
                members_selected.append("user_" + org_user["objectGUID"][0])

        for org_group in org_groups:
            members_choices.append(
                (
                    "group_" + org_group["objectGUID"][0],
                    _("Group") + ": " + org_group["name"][0],
                )
            )
            if org_group["distinguishedName"][0].upper() in (
                dn.upper() for dn in self.group_attributes["member"]
            ):
                members_selected.append("group_" + org_group["objectGUID"][0])

        for org_computer in org_computers:
            members_choices.append(
                (
                    "computer_" + org_computer["objectGUID"][0],
                    _("Computer") + ": " + org_computer["name"][0],
                )
            )
            if org_computer["distinguishedName"][0].upper() in (
                dn.upper() for dn in self.group_attributes["member"]
            ):
                members_selected.append("computer_" + org_computer["objectGUID"][0])

        self.fields["members"].choices = members_choices
        self.fields["members"].initial = members_selected


class DirectoryUserGroupMembershipForm(UserKwargModelFormMixin, forms.Form):
    groups = forms.MultipleChoiceField(
        required=False,
        widget=forms.SelectMultiple,
        label="",
    )

    def __init__(self, *args, **kwargs):
        self.user_attributes = kwargs.pop("user_attributes", None)

        super(DirectoryUserGroupMembershipForm, self).__init__(*args, **kwargs)

        # make sure, under no circumstances, user is None
        assert self.user is not None

        mdat = get_mdat_customer_for_user(self.user)

        group_choices = list()
        group_selected = list()

        all_groups = directory_backend_get_groups(mdat.id)

        for group in all_groups:
            group_choices.append(f"group_{group['X-GUID']} : {group['name'][0]}")
            if group["distinguishedName"][0] in self.user_attributes["memberOf"]:
                group_selected.append(f"group_{group['X-GUID']}")

        self.fields["groups"].choices = group_choices
        self.fields["groups"].initial = group_selected


class DirectoryComputerGroupMembershipForm(UserKwargModelFormMixin, forms.Form):
    groups = forms.MultipleChoiceField(
        required=False,
        widget=forms.SelectMultiple,
        label="",
    )

    def __init__(self, *args, **kwargs):
        self.computer_attributes = kwargs.pop("computer_attributes", None)

        super(DirectoryComputerGroupMembershipForm, self).__init__(*args, **kwargs)

        # make sure, under no circumstances, computer is None
        assert self.user is not None

        mdat = get_mdat_customer_for_user(self.user)

        group_choices = list()
        group_selected = list()

        all_groups = directory_backend_get_groups(mdat.id)

        for group in all_groups:
            group_choices.append(
                ("group_" + group["guid"], _("Group") + ": " + group["name"])
            )
            if group["dn"].upper() in (
                dn.upper() for dn in self.computer_attributes["memberOf"]
            ):
                group_selected.append("group_" + group["guid"])

        self.fields["groups"].choices = group_choices
        self.fields["groups"].initial = group_selected


class DirectoryUserProfileInformation(UserKwargModelFormMixin, forms.Form):
    userprincipalname = forms.CharField(
        label=_("Username"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Username",
                "readonly": "true",
            }
        ),
        required=False,
    )
    displayname = forms.CharField(
        label=_("Full Name"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Full Name",
                "readonly": "true",
            }
        ),
        required=False,
    )
    mail = forms.CharField(
        label=_("Email"),
        widget=forms.TextInput(
            attrs={"class": "form-control", "placeholder": "Email", "readonly": "true"}
        ),
        required=False,
    )
    postalcode = forms.CharField(
        label=_("Postalcode"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Postalcode",
                "readonly": "true",
            }
        ),
        required=False,
    )
    l = forms.CharField(
        label=_("City"),
        widget=forms.TextInput(
            attrs={"class": "form-control", "placeholder": "City", "readonly": "true"}
        ),
        required=False,
    )
    streetaddress = forms.CharField(
        label=_("Street Address"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Street Address",
                "readonly": "true",
            }
        ),
        required=False,
    )
    company = forms.CharField(
        label=_("Company"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Company",
                "readonly": "true",
            }
        ),
        required=False,
    )
